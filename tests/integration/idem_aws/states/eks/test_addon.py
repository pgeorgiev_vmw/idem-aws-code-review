import copy
import time
import uuid
from collections import ChainMap

import pytest


@pytest.mark.localstack(pro=False)
@pytest.mark.asyncio
async def test_eks_addon(hub, ctx, aws_eks_cluster, aws_iam_role):
    if hub.tool.utils.is_running_localstack(ctx):
        return

    cluster_id = aws_eks_cluster.get("resource_id")
    await is_cluster_active(hub, ctx, cluster_id)
    addon_temp_name = "kube-proxy"
    add_on_version = "v1.20.7-eksbuild.1"
    tags = {"Name": addon_temp_name}

    # create addon with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    ret = await hub.states.aws.eks.addon.present(
        test_ctx,
        name=addon_temp_name,
        cluster_name=cluster_id,
        addon_version=add_on_version,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert f"Would create aws.eks.addon '{addon_temp_name}'" in ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert_addon(resource, cluster_id, addon_temp_name, add_on_version, tags)

    # create addons
    ret = await hub.states.aws.eks.addon.present(
        ctx,
        name=addon_temp_name,
        cluster_name=cluster_id,
        addon_version=add_on_version,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert f"Created aws.eks.addon '{addon_temp_name}'" in ret["comment"]
    resource = ret.get("new_state")
    assert_addon(resource, cluster_id, addon_temp_name, add_on_version, tags)
    resource_id = resource.get("resource_id")

    # Describe kube-proxy addon
    describe_ret = await hub.states.aws.eks.addon.describe(ctx)
    assert f"{cluster_id}-{resource_id}" in describe_ret

    # Verify that the describe output format is correct
    assert "aws.eks.addon.present" in describe_ret.get(f"{cluster_id}-{resource_id}")
    described_resource = describe_ret.get(f"{cluster_id}-{resource_id}").get(
        "aws.eks.addon.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert_addon(
        described_resource_map,
        cluster_id,
        f"{cluster_id}-{addon_temp_name}",
        add_on_version,
        tags,
    )
    # Test updating tags and addon_version and service_account_role_arn
    new_service_account_role_arn = aws_iam_role.get("arn")
    tags = {
        "Name": addon_temp_name,
        f"idem-test-addon-key-{str(uuid.uuid4())}": f"idem-test-addon-value-{str(uuid.uuid4())}",
    }
    new_add_on_version = "v1.21.2-eksbuild.2"
    # update addon with test flag
    ret = await hub.states.aws.eks.addon.present(
        test_ctx,
        name=addon_temp_name,
        resource_id=resource_id,
        cluster_name=cluster_id,
        addon_version=new_add_on_version,
        service_account_role_arn=new_service_account_role_arn,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert f"Would update aws.eks.addon '{addon_temp_name}'" in ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")

    assert_addon(resource, cluster_id, addon_temp_name, new_add_on_version, tags)
    assert new_service_account_role_arn == resource.get("service_account_role_arn")

    # update addon in real
    ret = await hub.states.aws.eks.addon.present(
        ctx,
        name=addon_temp_name,
        resource_id=resource_id,
        cluster_name=cluster_id,
        addon_version=new_add_on_version,
        service_account_role_arn=new_service_account_role_arn,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert_addon(resource, cluster_id, addon_temp_name, new_add_on_version, tags)
    assert new_service_account_role_arn == resource.get("service_account_role_arn")

    # Delete addon with test flag
    ret = await hub.states.aws.eks.addon.absent(
        test_ctx, name=addon_temp_name, cluster_name=cluster_id, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert f"Would delete aws.eks.addon '{addon_temp_name}'" in ret["comment"]

    # Delete addons
    ret = await hub.states.aws.eks.addon.absent(
        ctx, name=addon_temp_name, cluster_name=cluster_id, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert f"Deleted aws.eks.addon '{addon_temp_name}'" in ret["comment"]

    # Deleting addon again should be a no-op
    ret = await hub.states.aws.eks.addon.absent(
        ctx, name=addon_temp_name, cluster_name=cluster_id, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert (not ret.get("old_state")) and (not ret.get("new_state"))
    assert f"aws.eks.addon '{addon_temp_name}' already absent" in ret["comment"]


async def is_cluster_active(hub, ctx, resource_id):
    for i in range(20):
        resource = await hub.exec.boto3.client.eks.describe_cluster(
            ctx, name=resource_id
        )
        if resource["result"] and resource["ret"]:
            cluster = resource["ret"]["cluster"]
            status = cluster["status"]
            if status != "ACTIVE":
                time.sleep(180)
            else:
                break


def assert_addon(resource, cluster_id, addon_temp_name, add_on_version, tags):
    assert cluster_id == resource.get("cluster_name")
    assert addon_temp_name == resource.get("name")
    assert add_on_version == resource.get("addon_version")
    assert tags == resource.get("tags")
