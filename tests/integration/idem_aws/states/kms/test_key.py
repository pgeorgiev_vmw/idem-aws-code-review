import copy
import os
import uuid

import pytest


@pytest.mark.asyncio
async def test_key(hub, ctx):
    # Get account from environment
    CI_ACCT_NUM = os.getenv("CI_ACCT_NUM")
    if not hub.tool.utils.is_running_localstack(ctx):
        assert (
            CI_ACCT_NUM
        ), "AWS account number has to be supplied as CI_ACCT_NUM to run this test."
    CI_ACCT_NUM = CI_ACCT_NUM if CI_ACCT_NUM else "6767676767"

    print(f"Going to use account '{CI_ACCT_NUM}' for KMS key")
    # Create key
    name = "idem-test-key-" + str(uuid.uuid4())
    tags = [{"TagKey": "Name", "TagValue": name}]
    ret = await hub.states.aws.kms.key.present(
        ctx,
        name=name,
        description=f"Description for {name}",
        policy='{ "Version" : "2012-10-17",  "Id" : "key-consolepolicy-3", '
        '"Statement" : [{"Sid" : "Enable IAM User Permissions","Effect" : "Allow", '
        '"Principal" : {"AWS" : "arn:aws:iam::'
        f"{CI_ACCT_NUM}"
        ':root"},"Action" : [ "kms:Create*", "kms:Describe*", '
        '"kms:Enable*", "kms:List*", "kms:Put*", "kms:Update*", "kms:ScheduleKeyDeletion", "kms:Revoke*", '
        '"kms:Disable*", "kms:Get*", "kms:Delete*", "kms:TagResource", "kms:UntagResource" ], "Resource" : "*" }]}',
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state")
    assert ret.get("new_state")
    resource = ret.get("new_state")
    assert f"Description for {name}" == resource.get("description")
    resource_id = resource.get("resource_id", None)
    assert resource_id
    assert tags == resource.get("tags")
    assert ret["new_state"].get("enable_key_rotation") is False

    # Create key with "test" flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    test_name = name + "-test"
    ret = await hub.states.aws.kms.key.present(
        test_ctx,
        name=test_name,
        description=f"Description for {test_name}",
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert "Would create aws.kms.key" in str(ret["comment"])
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert tags == resource.get("tags")
    assert test_name == resource.get("name")

    # Update w/o changes with "test" flag
    ret = await hub.states.aws.kms.key.present(
        test_ctx,
        name=name,
        resource_id=resource_id,
        description=f"Description for {name}",
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert "already exists" in str(ret["comment"])

    # Update key tags, state, policy and description with "test" flag
    tags = [
        {"TagKey": "Name", "TagValue": test_name},
        {"TagKey": "test-1", "TagValue": "test-1-val"},
        {"TagKey": "test-2", "TagValue": "test-2-val"},
    ]
    ret = await hub.states.aws.kms.key.present(
        test_ctx,
        name=name,
        resource_id=resource_id,
        description=f"Description for {test_name}",
        tags=tags,
        key_state="Disabled",
        policy='{ "Version" : "2012-10-17",  "Id" : "key-consolepolicy-3", '
        '"Statement" : [{"Sid" : "Enable IAM User Permissions","Effect" : "Allow", '
        '"Principal" : {"AWS" : "arn:aws:iam::'
        f"{CI_ACCT_NUM}"
        ':root"},"Action" : "kms:*", "Resource" : "*" }]}',
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert "Would update" in str(ret["comment"])
    resource = ret.get("new_state")
    assert resource.get("key_state") == "Disabled"
    assert tags == resource.get("tags")
    assert ret.get("old_state").get("policy") != ret.get("new_state").get("policy")
    assert f"Description for {test_name}" == ret.get("new_state").get("description")
    assert resource_id == resource.get("resource_id")

    # # Delete with "test" flag
    ret = await hub.states.aws.kms.key.absent(
        test_ctx,
        name=name,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert "Would schedule deletion" in str(ret["comment"])

    # Update tags
    tags = [
        {"TagKey": "Name", "TagValue": name},
        {"TagKey": "test-1", "TagValue": "test-1-val"},
        {"TagKey": "test-2", "TagValue": "test-2-val"},
    ]
    ret = await hub.states.aws.kms.key.present(
        ctx,
        name=name,
        resource_id=resource_id,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert "Update" in str(ret["comment"])
    assert "tags" in str(ret["comment"])
    resource = ret.get("new_state")
    assert resource.get("tags") == tags, ret["comment"]

    # Commenting since account has unauthorized keys that fail describe
    # Verify update with tags using describe
    # describe_ret = await hub.states.aws.kms.key.describe(ctx)
    # assert resource_id in describe_ret
    # hub.tool.utils.verify_in_list(
    #     describe_ret[resource_id]["aws.kms.key.present"], "tags", tags
    # )

    # Update key description and set policy
    ret = await hub.states.aws.kms.key.present(
        ctx,
        name=name,
        resource_id=resource_id,
        description=f"Description for {test_name}",
        tags=tags,
        key_state="Enabled",
        enable_key_rotation="True",
        policy='{ "Version" : "2012-10-17",  "Id" : "key-consolepolicy-3", '
        '"Statement" : [{"Sid" : "Enable IAM User Permissions","Effect" : "Allow", '
        '"Principal" : {"AWS" : "arn:aws:iam::'
        f"{CI_ACCT_NUM}"
        ':root"},"Action" : "kms:*", "Resource" : "*" }]}',
    )
    assert ret["result"], ret["comment"]
    assert "Updated" in str(ret["comment"])
    assert "policy" in str(ret["comment"])
    assert "description" in str(ret["comment"])
    assert f"Description for {test_name}" == ret.get("new_state").get("description")
    assert ret.get("old_state").get("policy") != ret.get("new_state").get("policy")
    assert ret.get("new_state").get("enable_key_rotation", False) is True

    # Disable key
    ret = await hub.states.aws.kms.key.present(
        ctx,
        name=name,
        resource_id=resource_id,
        key_state="Disabled",
    )
    assert ret["result"], ret["comment"]
    assert ret["new_state"].get("key_state") == "Disabled"

    # Enable key
    ret = await hub.states.aws.kms.key.present(
        ctx, name=name, resource_id=resource_id, key_state="Enabled"
    )
    assert ret["result"], ret["comment"]
    assert ret["new_state"].get("key_state") == "Enabled"

    # Delete Key - schedule for deletion with default value (7 days)
    ret = await hub.states.aws.kms.key.absent(ctx, name=name, resource_id=resource_id)
    assert ret["result"], ret["comment"]
    assert ret.get("new_state").get("key_state", None) == "PendingDeletion"


@pytest.mark.asyncio
async def test_kms_key_absent_with_none_resource_id(hub, ctx):
    name = "idem-test-key-" + str(uuid.uuid4())
    # Delete security group with resource_id as None. Result in no-op.
    ret = await hub.states.aws.kms.key.absent(ctx, name=name, resource_id=None)
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.kms.key", name=name
        )[0]
        in ret["comment"]
    )
