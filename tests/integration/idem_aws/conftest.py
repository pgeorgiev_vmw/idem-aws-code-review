import copy
import json
import os
import pathlib
import random
import string
import tempfile
import time
import uuid
import zipfile
from typing import Any
from typing import ByteString
from typing import Dict
from typing import Tuple

import pytest
from Cryptodome.PublicKey import RSA


# ================================================================================
# AWS resource fixtures
# ================================================================================
@pytest.fixture(scope="module")
async def aws_ec2_subnet(hub, ctx, aws_ec2_vpc) -> Dict[str, Any]:
    """
    Create and cleanup an Ec2 subnet for a module that needs it
    :return: a description of an ec2 subnet
    """
    subnet_temp_name = "idem-fixture-subnet-" + str(uuid.uuid4())
    az = ctx["acct"].get("region_name") + "b"
    ipv4_cidr_sub_block = hub.tool.utils.get_sub_cidr_block(
        aws_ec2_vpc.get("CidrBlock"), 28
    )
    ret = await hub.states.aws.ec2.subnet.present(
        ctx,
        name=subnet_temp_name,
        cidr_block=ipv4_cidr_sub_block,
        vpc_id=aws_ec2_vpc.get("VpcId"),
        availability_zone=az,
        tags=[{"Key": "Name", "Value": subnet_temp_name}],
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = hub.tool.boto3.resource.create(
        ctx, "ec2", "Subnet", ret.get("new_state").get("resource_id")
    )
    after = await hub.tool.boto3.resource.describe(resource)

    yield after

    ret = await hub.states.aws.ec2.subnet.absent(
        ctx, name=subnet_temp_name, resource_id=after["SubnetId"]
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest.fixture(scope="module")
async def aws_ec2_subnet_2(hub, ctx, aws_ec2_vpc) -> Dict[str, Any]:
    """
    Create and cleanup an Ec2 subnet for a module that needs it
    :return: a description of an ec2 subnet
    """
    subnet_temp_name = "idem-fixture-subnet-" + str(uuid.uuid4())
    az = ctx["acct"].get("region_name") + "a"
    ipv4_cidr_sub_block = hub.tool.utils.get_sub_cidr_block(
        aws_ec2_vpc.get("CidrBlock"), 28
    )
    ret = await hub.states.aws.ec2.subnet.present(
        ctx,
        name=subnet_temp_name,
        cidr_block=ipv4_cidr_sub_block,
        vpc_id=aws_ec2_vpc.get("VpcId"),
        availability_zone=az,
        tags=[{"Key": "Name", "Value": subnet_temp_name}],
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = hub.tool.boto3.resource.create(
        ctx, "ec2", "Subnet", ret.get("new_state").get("resource_id")
    )
    after = await hub.tool.boto3.resource.describe(resource)

    yield after

    ret = await hub.states.aws.ec2.subnet.absent(
        ctx, name=subnet_temp_name, resource_id=after["SubnetId"]
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest.fixture(scope="module")
async def aws_ec2_vpc(hub, ctx) -> Dict[str, Any]:
    """
    Create and cleanup an Ec2 vpc for a module that needs it
    :return: a description of an ec2 vpc
    """
    vpc_temp_name = "idem-fixture-vpc-" + str(uuid.uuid4())
    cidr_block = os.getenv("IT_FIXTURE_EC2_VPC_CIDR_BLOCK")
    num: () = lambda: random.randint(0, 255)
    if cidr_block is None:
        cidr_block = f"192.168.{num()}.0/24"
    cidr_block_association_set = [{"CidrBlock": cidr_block}]
    ret = await hub.states.aws.ec2.vpc.present(
        ctx,
        name=vpc_temp_name,
        cidr_block_association_set=copy.deepcopy(cidr_block_association_set),
        tags=[{"Key": "Name", "Value": vpc_temp_name}],
        enable_dns_hostnames=True,
        enable_dns_support=True,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = hub.tool.boto3.resource.create(
        ctx, "ec2", "Vpc", ret.get("new_state").get("resource_id")
    )
    after = await hub.tool.boto3.resource.describe(resource)
    assert after

    yield after

    ret = await hub.states.aws.ec2.vpc.absent(
        ctx, name=vpc_temp_name, resource_id=after["VpcId"]
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest.fixture(scope="module")
async def aws_ec2_vpc_2(hub, ctx) -> Dict[str, Any]:
    """
    Create and cleanup an Ec2 vpc for a module that needs it
    :return: a description of an ec2 vpc
    """
    vpc_temp_name = "idem-fixture-vpc-" + str(uuid.uuid4())
    cidr_block = os.getenv("IT_FIXTURE_EC2_VPC_CIDR_BLOCK")
    num: () = lambda: random.randint(0, 255)
    if cidr_block is None:
        cidr_block = f"192.168.{num()}.0/24"
    cidr_block_association_set = [{"CidrBlock": cidr_block}]
    ret = await hub.states.aws.ec2.vpc.present(
        ctx,
        name=vpc_temp_name,
        cidr_block_association_set=copy.deepcopy(cidr_block_association_set),
        tags=[{"Key": "Name", "Value": vpc_temp_name}],
        enable_dns_hostnames=True,
        enable_dns_support=True,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = hub.tool.boto3.resource.create(
        ctx, "ec2", "Vpc", ret.get("new_state").get("resource_id")
    )
    after = await hub.tool.boto3.resource.describe(resource)
    assert after

    yield after

    ret = await hub.states.aws.ec2.vpc.absent(
        ctx, name=vpc_temp_name, resource_id=after["VpcId"]
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest.fixture(scope="module")
async def aws_ec2_transit_gateway(hub, ctx) -> Dict[str, Any]:
    """
    Create and cleanup an Ec2 transit gateway for a module that needs it
    :return: a description of an ec2 transit gateway
    """
    transit_gateway_temp_name = "idem-fixture-transit-gateway-" + str(uuid.uuid4())
    transit_gateway_tags = [{"Key": "Name", "Value": transit_gateway_temp_name}]
    description = "idem-fixture-transit-gateway"
    ret = await hub.states.aws.ec2.transit_gateway.present(
        ctx,
        name=transit_gateway_temp_name,
        description=description,
        tags=transit_gateway_tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")
    resource_id = after.get("resource_id")
    await hub.tool.ec2_test_util.wait_for_transit_gateway_state(
        ctx, resource_id, "available"
    )

    yield after

    ret = await hub.states.aws.ec2.transit_gateway.absent(
        ctx, name=transit_gateway_temp_name, resource_id=after["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    await hub.tool.ec2_test_util.wait_for_transit_gateway_state(
        ctx, resource_id, "deleted"
    )


@pytest.fixture(scope="module")
async def aws_ec2_nat_gateway(hub, ctx, aws_ec2_subnet) -> Dict[str, Any]:
    """
    Create and cleanup an EC2 NAT gateway for a module that needs it
    :return: a description of an EC2 NAT Gateway
    """
    nat_gateway_name = "idem-fixture-nat-gateway-" + str(uuid.uuid4())
    tags = [
        {"Key": "Name", "Value": nat_gateway_name},
    ]
    ret = await hub.states.aws.ec2.nat_gateway.present(
        ctx,
        name=nat_gateway_name,
        subnet_id=aws_ec2_subnet.get("SubnetId"),
        client_token=None,
        connectivity_type="private",
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")
    resource_id = after.get("resource_id")
    yield after

    # Delete instance
    ret = await hub.states.aws.ec2.nat_gateway.absent(
        ctx,
        name=nat_gateway_name,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and (not ret.get("new_state"))


@pytest.fixture(scope="module")
async def aws_ec2_security_group(hub, ctx, aws_ec2_vpc) -> Dict[str, Any]:
    """
    Create and cleanup an Ec2 security group for a module that needs it
    :return: a description of an ec2 security group
    """
    security_group_temp_name = "idem-fixture-security-group-" + str(uuid.uuid4())
    security_group_tags = [{"Key": "Name", "Value": security_group_temp_name}]
    description = "Security group fixture resource for Idem integration test."
    ret = await hub.states.aws.ec2.security_group.present(
        ctx,
        name=security_group_temp_name,
        description=description,
        vpc_id=aws_ec2_vpc.get("VpcId"),
        tags=security_group_tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")

    yield after

    ret = await hub.states.aws.ec2.security_group.absent(
        ctx, name=security_group_temp_name, resource_id=after["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest.fixture(scope="module")
async def aws_ec2_internet_gateway(hub, ctx, aws_ec2_vpc) -> Dict[str, Any]:
    """
    Create and cleanup an Ec2 internet gateway for a module that needs it
    :return: a description of an ec2 internet gateway
    """
    internet_gateway_temp_name = "idem-fixture-internet-gateway-" + str(uuid.uuid4())
    internet_gateway_tags = [{"Key": "Name", "Value": internet_gateway_temp_name}]
    ret = await hub.states.aws.ec2.internet_gateway.present(
        ctx,
        name=internet_gateway_temp_name,
        vpc_id=[aws_ec2_vpc.get("VpcId")],
        tags=internet_gateway_tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")

    yield after

    ret = await hub.states.aws.ec2.internet_gateway.absent(
        ctx, name=internet_gateway_temp_name, resource_id=after["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest.fixture(scope="module")
async def aws_iam_role(hub, ctx) -> Dict[str, Any]:
    """
    Create and cleanup an IAM role for a module that needs it
    :return: a description of an IAM role
    """
    role_name = "idem-fixture-role-" + str(uuid.uuid4())
    assume_role_policy_document = '{"Version": "2012-10-17","Statement": {"Effect": "Allow","Principal": {"Service": [ "spot.amazonaws.com", "lambda.amazonaws.com" ]},"Action": "sts:AssumeRole"}}'
    description = "Idem IAM role integration test fixture"
    ret = await hub.states.aws.iam.role.present(
        ctx,
        name=role_name,
        assume_role_policy_document=assume_role_policy_document,
        description=description,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")

    yield after

    # role should be deleted by name (not resource_id)
    ret = await hub.states.aws.iam.role.absent(
        ctx, name=after["name"], resource_id=after["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest.fixture(scope="module")
async def aws_iam_role_2(hub, ctx) -> Dict[str, Any]:
    """
    Create and cleanup an IAM role for a module that needs it
    :return: a description of an IAM role
    """
    role_name = "idem-fixture-role-" + str(uuid.uuid4())
    assume_role_policy_document = '{"Version": "2012-10-17","Statement": {"Effect": "Allow","Principal": {"Service": ["spot.amazonaws.com", "lambda.amazonaws.com", "events.amazonaws.com"]},"Action": "sts:AssumeRole"}}'
    description = "Idem IAM role integration test fixture 2"
    ret = await hub.states.aws.iam.role.present(
        ctx,
        name=role_name,
        assume_role_policy_document=assume_role_policy_document,
        description=description,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")

    yield after

    # role should be deleted by name (not resource_id)
    ret = await hub.states.aws.iam.role.absent(
        ctx, name=after["name"], resource_id=after["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest.fixture(scope="module")
async def aws_iam_user(hub, ctx) -> Dict[str, Any]:
    """
    Create and cleanup an IAM user for a module that needs it
    :return: a description of an IAM user
    """
    user_name = "idem-fixture-user-" + str(uuid.uuid4())
    ret = await hub.states.aws.iam.user.present(
        ctx, name=user_name, user_name=user_name
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")

    yield after

    ret = await hub.states.aws.iam.user.absent(
        ctx, name=after.get("name"), resource_id=after.get("resource_id")
    )
    assert ret["result"], ret["comment"]


@pytest.fixture(scope="module")
async def aws_iam_user_2(hub, ctx) -> Dict[str, Any]:
    """
    Create and cleanup an IAM user for a module that needs it
    :return: a description of an IAM user
    """
    user_name = "idem-fixture-user-" + str(uuid.uuid4())
    ret = await hub.states.aws.iam.user.present(
        ctx, name=user_name, user_name=user_name
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")

    yield after

    ret = await hub.states.aws.iam.user.absent(
        ctx, name=after.get("name"), resource_id=after.get("resource_id")
    )
    assert ret["result"], ret["comment"]


@pytest.fixture(scope="function")
async def aws_lambda_function(hub, ctx, zip_file, aws_iam_role_2) -> Dict[str, Any]:
    function_name = "test_idem_aws_function_" + "".join(
        random.choice(string.ascii_lowercase + string.digits) for _ in range(20)
    )

    # this is to ensure the created role 'aws_iam_role_2' is fully functional and is ready to be
    # assumed by Lambda service. Since role doesn't have a status field, tests are repeatedly failing
    # with exception: 'InvalidParameterValueException: An error occurred (InvalidParameterValueException) when calling
    #                  the CreateFunction operation: The role defined for the function cannot be assumed by Lambda.',)
    if not hub.tool.utils.is_running_localstack(ctx):
        time.sleep(45)

    ret = await hub.states.aws.lambda_.function.present(
        ctx,
        name=function_name,
        runtime="python3.7",
        handler="code.main",
        role=aws_iam_role_2["arn"],
        code={"ZipFile": zip_file},
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")

    yield after

    ret = await hub.states.aws.lambda_.function.absent(
        ctx, name=function_name, resource_id=function_name
    )
    assert ret["result"], ret["comment"]
    assert f"Deleted aws.lambda.function '{function_name}'" in ret["comment"]


@pytest.fixture(scope="module")
async def aws_kms_key(hub, ctx) -> Dict[str, Any]:
    """
    Create and cleanup an KMS key for a module that needs it
    :return: a description of an KMS key
    """
    CI_ACCT_NUM = os.getenv("CI_ACCT_NUM")
    if not hub.tool.utils.is_running_localstack(ctx):
        assert (
            CI_ACCT_NUM
        ), "AWS account number has to be supplied as CI_ACCT_NUM to run this test."
    CI_ACCT_NUM = CI_ACCT_NUM if CI_ACCT_NUM else "6767676767"
    print(f"Going to use account '{CI_ACCT_NUM}' for KMS key")

    key_name = "idem-fixture-kms-key-" + str(uuid.uuid4())
    ret = await hub.states.aws.kms.key.present(
        ctx,
        key_name,
        policy='{ "Version" : "2012-10-17",  "Id" : "key-consolepolicy-3", '
        '"Statement" : [{"Sid" : "Enable IAM User Permissions","Effect" : "Allow", '
        '"Principal" : {"AWS" : "arn:aws:iam::'
        f"{CI_ACCT_NUM}"
        ':root"},"Action" : [ "kms:Create*", "kms:Describe*", '
        '"kms:Enable*", "kms:List*", "kms:Put*", "kms:Update*", "kms:ScheduleKeyDeletion", "kms:Revoke*", '
        '"kms:Disable*", "kms:Get*", "kms:Delete*", "kms:TagResource", "kms:UntagResource" ], "Resource" : "*" }]}',
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")

    yield after
    ret = await hub.states.aws.kms.key.absent(
        ctx, name=key_name, resource_id=after["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state")


@pytest.fixture(scope="module")
async def aws_s3_bucket(hub, ctx) -> Dict[str, Any]:
    bucket_name = "idem-fixture-bucket-" + str(uuid.uuid4())

    ret = await hub.states.aws.s3.bucket.present(
        ctx,
        name=bucket_name,
        create_bucket_configuration={
            "LocationConstraint": ctx["acct"].get("region_name")
        },
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    bucket = ret.get("new_state")

    yield bucket

    # S3 Bucket is not getting deleted until it is empty, so deleted all the objects in bucket first.
    await deleteBucketObjects(hub, ctx, bucket_name)

    ret = await hub.states.aws.s3.bucket.absent(ctx, name=bucket_name)
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


async def deleteBucketObjects(hub, ctx, bucket_name: str):
    ret = await hub.exec.boto3.client.s3.list_objects(ctx, Bucket=bucket_name)
    if ret["result"]:
        for bucket_object in ret["ret"]["Contents"]:
            key = bucket_object.get("Key")
            result = await hub.exec.boto3.client.s3.delete_object(
                ctx, Bucket=bucket_name, Key=key
            )
            if not result["result"]:
                hub.log.error({ret["comment"]})
    else:
        hub.log.error({ret["comment"]})


@pytest.fixture(scope="module")
async def aws_route53_private_hosted_zone(hub, ctx, aws_ec2_vpc) -> Dict[str, Any]:
    # localstack is not supporting creation of private hosted zones. It gives error 500 Internal server error.
    # We can only attach vpc to private hosted zones. so this test can be only run on real AWS.
    if hub.tool.utils.is_running_localstack(ctx):
        yield None
    else:
        name = "idem-fixture-hosted-zone-" + str(uuid.uuid4())

        # create hosted zone

        result = await hub.states.aws.route53.hosted_zone.present(
            ctx,
            name=name,
            caller_reference=str(uuid.uuid4()),
            hosted_zone_name=name,
            config={"PrivateZone": True},
            vpcs={
                "VPCId": aws_ec2_vpc.get("VpcId"),
                "VPCRegion": ctx["acct"].get("region_name"),
            },
        )
        assert result["result"], result["comment"]
        after = result.get("new_state")

        yield after

        resource_id = after.get("resource_id")
        ret = await hub.states.aws.route53.hosted_zone.absent(
            ctx, name=name, resource_id=resource_id
        )

        assert ret["comment"]
        assert ret.get("old_state") and not ret.get("new_state")


@pytest.fixture(scope="module")
async def aws_organization(hub, ctx) -> Dict[str, Any]:
    name = "idem-fixture-aws-organization-" + str(uuid.uuid4())

    # create organization
    result = await hub.states.aws.organizations.organization.present(
        ctx, name=name, feature_set="ALL"
    )

    assert result["result"], result["comment"]
    after = result.get("new_state")

    yield after

    ret = await hub.states.aws.organizations.organization.absent(ctx, name=name)

    assert ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest.fixture(scope="module")
async def aws_organization_unit(hub, ctx, aws_organization) -> Dict[str, Any]:
    ou_name = "idem-fixture-organization-unit-" + str(uuid.uuid4())
    roots_resp = await hub.exec.boto3.client.organizations.list_roots(ctx)

    root_id = None

    if roots_resp:
        root_id = roots_resp["ret"]["Roots"][0]["Id"]

    create_tag = [{"Key": "org", "Value": "idem"}, {"Key": "env", "Value": "test"}]
    # create organization unit
    result = await hub.states.aws.organizations.organization_unit.present(
        ctx, name=ou_name, parent_id=root_id, tags=create_tag
    )

    assert result["result"], result["comment"]
    after = result.get("new_state")

    yield after

    ret = await hub.states.aws.organizations.organization_unit.absent(
        ctx,
        name=result.get("new_state").get("name"),
        resource_id=result.get("new_state").get("resource_id"),
    )

    assert ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest.fixture(scope="module")
async def aws_organization_policy(hub, ctx, aws_organization) -> Dict[str, Any]:
    policy_name = "idem-fixture-organizations-policy-" + str(uuid.uuid4())
    description = "Enables admins of attached accounts to delegate all S3 permissions"
    policy_type = "SERVICE_CONTROL_POLICY"
    content = """{
      "Version": "2012-10-17",
      "Statement": [
        {
          "Action": [
            "s3:DeleteBucket",
            "s3:DeleteObject",
            "s3:DeleteObjectVersion"
          ],
          "Resource": "*",
          "Effect": "Deny"
        }
      ]
    }"""

    create_tag = [{"Key": "org", "Value": "idem"}]

    result = await hub.states.aws.organizations.policy.present(
        ctx,
        name=policy_name,
        description=description,
        policy_type=policy_type,
        content=content,
        tags=create_tag,
    )
    if not hub.tool.utils.is_running_localstack(ctx):
        assert result["result"], result["comment"]
    after = result.get("new_state")

    yield after

    ret = await hub.states.aws.organizations.policy.absent(
        ctx,
        name=result.get("new_state").get("name"),
        resource_id=result.get("new_state").get("resource_id"),
    )

    assert ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest.fixture(scope="module")
async def aws_sns_topic(hub, ctx) -> Dict[str, Any]:
    topic_name = "idem-fixture-topic-" + str(uuid.uuid4())
    delivery_policy = json.dumps(
        {
            "http": {
                "defaultHealthyRetryPolicy": {
                    "minDelayTarget": 10,
                    "maxDelayTarget": 30,
                    "numRetries": 10,
                    "numMaxDelayRetries": 7,
                    "numNoDelayRetries": 0,
                    "numMinDelayRetries": 3,
                    "backoffFunction": "linear",
                },
                "disableSubscriptionOverrides": False,
            }
        },
        separators=(",", ":"),
    )
    attributes = {"DeliveryPolicy": delivery_policy}
    tags = [{"Key": "Name", "Value": topic_name}]

    ret = await hub.states.aws.sns.topic.present(
        ctx, name=topic_name, attributes=attributes, tags=tags
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    new_state = ret.get("new_state")

    yield new_state

    ret = await hub.states.aws.sns.topic.absent(
        ctx, name=topic_name, resource_id=new_state.get("resource_id")
    )
    assert ret["result"], ret["comment"]
    assert f"Deleted aws.sns.topic '{topic_name}'" in ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest.fixture(scope="module")
async def aws_rds_db_subnet_group(hub, ctx, aws_ec2_subnet, aws_ec2_subnet_2):
    # To skip running on localstack as it is only working with real aws
    if hub.tool.utils.is_running_localstack(ctx):
        yield None
    else:
        # Create db_subnet_group
        db_subnet_group_name = "idem-fixture-db-subnet-group-" + str(uuid.uuid4())
        tags = [
            {"Key": "Name", "Value": db_subnet_group_name},
        ]
        ret = await hub.states.aws.rds.db_subnet_group.present(
            ctx,
            name=db_subnet_group_name,
            db_subnet_group_description="idem fixture",
            subnets=[
                aws_ec2_subnet.get("SubnetId"),
                aws_ec2_subnet_2.get("SubnetId"),
            ],
            tags=tags,
        )
        assert ret["result"], ret["comment"]
        assert not ret.get("old_state") and ret.get("new_state")
        assert f"Created '{db_subnet_group_name}'" in ret["comment"]
        new_state = ret["new_state"]

        yield new_state

        ret = await hub.states.aws.rds.db_subnet_group.absent(
            ctx, name=db_subnet_group_name, resource_id=new_state["resource_id"]
        )
        assert ret["result"], ret["comment"]
        assert f"Deleted '{db_subnet_group_name}'" in ret["comment"]
        assert ret.get("old_state") and not ret.get("new_state")


@pytest.fixture(scope="module")
async def aws_rds_db_cluster(hub, ctx, aws_rds_db_subnet_group) -> Dict[str, Any]:
    # To skip running on localstack as it is only working with real aws
    if hub.tool.utils.is_running_localstack(ctx):
        yield None
    else:
        name = "idem-fixture-db-cluster" + str(uuid.uuid4())
        # Create db_cluster
        db_subnet_group_name = aws_rds_db_subnet_group.get("resource_id")
        ret = await hub.states.aws.rds.db_cluster.present(
            ctx,
            name=name,
            engine="aurora-mysql",
            master_username="admin123",
            master_user_password="abcd1234",
            db_subnet_group_name=db_subnet_group_name,
        )
        assert ret["result"], ret["comment"]
        assert f"Created aws.rds.db_cluster '{name}'" in ret["comment"]
        assert not ret.get("old_state") and ret.get("new_state")
        new_state = ret.get("new_state")

        yield new_state

        ret = await hub.states.aws.rds.db_cluster.absent(
            ctx,
            name=name,
            skip_final_snapshot=True,
            resource_id=new_state["resource_id"],
        )
        assert ret["result"], ret["comment"]
        assert f"Deleted aws.rds.db_cluster '{name}'" in ret["comment"]
        assert ret.get("old_state") and not ret.get("new_state")


@pytest.fixture(scope="module")
async def aws_s3_bucket_policy(hub, ctx, aws_s3_bucket) -> Dict[str, Any]:
    bucket_name = aws_s3_bucket.get("name")
    bucket_policy_temp_name = "idem-fixture-bucket-policy-" + str(uuid.uuid4())

    policy_to_add = (
        '{"Version":"2012-10-17","Statement":[{"Sid":"AWSCloudTrailAclCheck20150319","Effect":"Allow","Principal":{"Service": "cloudtrail.amazonaws.com"},"Action":"s3:GetBucketAcl","Resource":"arn:aws:s3:::'
        + bucket_name
        + '"},{"Sid":"AWSCloudTrailWrite20150319","Effect":"Allow","Principal":{"Service": "cloudtrail.amazonaws.com"},"Action":"s3:PutObject", "Resource": "arn:aws:s3:::'
        + bucket_name
        + '/AWSLogs/*/*","Condition": {"StringEquals": {"s3:x-amz-acl": "bucket-owner-full-control"}}}]}'
    )
    result = await hub.states.aws.s3.bucket_policy.present(
        ctx,
        name=bucket_policy_temp_name,
        bucket=bucket_name,
        policy=policy_to_add,
    )

    assert result["result"], result["comment"]
    after = result.get("new_state")

    yield after

    ret = await hub.states.aws.s3.bucket_policy.absent(
        ctx, name=after.get("Name"), bucket=aws_s3_bucket.get("name")
    )

    assert ret["comment"]
    assert ret.get("old_state")


@pytest.fixture(scope="module")
async def aws_anomaly_monitor(hub, ctx) -> Dict[str, Any]:
    # To skip running on localstack as it is only working with real aws
    if hub.tool.utils.is_running_localstack(ctx):
        yield None
    else:
        monitor_name = "idem-fixture-anomaly-monitor-" + str(uuid.uuid4())
        monitor_type = "CUSTOM"
        monitor_specification = {
            "Dimensions": {"Key": "LINKED_ACCOUNT", "Values": ["123456789101"]}
        }

        ret = await hub.states.aws.costexplorer.anomaly_monitor.present(
            ctx,
            name=monitor_name,
            monitor_name=monitor_name,
            monitor_type=monitor_type,
            monitor_specification=monitor_specification,
        )
        assert ret["result"], ret["comment"]
        assert not ret.get("old_state") and ret.get("new_state")
        new_state = ret.get("new_state")

        yield new_state

        ret = await hub.states.aws.costexplorer.anomaly_monitor.absent(
            ctx, name=monitor_name, resource_id=new_state.get("resource_id")
        )
        assert ret["result"], ret["comment"]
        assert (
            f"Deleted aws.costexplorer.anomaly_monitor '{monitor_name}'"
            in ret["comment"]
        )
        assert ret.get("old_state") and not ret.get("new_state")


# --------------------------------------------------------------------------------

# ================================================================================
# resource helpers
# ================================================================================
@pytest.fixture(scope="module", name="instance_name")
def aws_instance_name() -> str:
    yield "test-idem-cloud-" + "".join(
        random.choice(string.ascii_lowercase + string.digits) for _ in range(20)
    )


@pytest.fixture(scope="module")
def ssh_key_pair() -> Tuple[ByteString, ByteString]:
    key_pair = RSA.generate(1024)
    yield key_pair.export_key(), key_pair.publickey().export_key()


@pytest.fixture(scope="module")
def zip_file() -> ByteString:
    """
    Create a zip file for tests that use lambda functions
    """
    with tempfile.TemporaryDirectory() as tempdir:
        path = pathlib.Path(tempdir)
        z_path = path.joinpath("code.zip")

        with zipfile.ZipFile(z_path, "w") as myzip:
            myzip.writestr("code.py", "def main():\n\treturn 0")

        with open(z_path, "rb") as fh:
            yield fh.read()


# --------------------------------------------------------------------------------
# ================================================================================
# Launch configuration - AMI image
# ================================================================================
@pytest.fixture(scope="module")
async def aws_image(hub, ctx) -> str:
    """
    Fetch ami image id
    :return: image id
    """
    image_id = "ami-73949613"
    if not hub.tool.utils.is_running_localstack(ctx):
        filters = [
            {"Name": "owner-alias", "Values": ["amazon"]},
            {"Name": "image-type", "Values": ["machine"]},
            {"Name": "architecture", "Values": ["x86_64"]},
            {"Name": "platform", "Values": ["windows"]},
            {"Name": "state", "Values": ["available"]},
            {"Name": "virtualization-type", "Values": ["hvm"]},
        ]
        resource = await hub.exec.boto3.client.ec2.describe_images(
            ctx,
            Filters=filters,
            IncludeDeprecated=False,
            DryRun=False,
        )

        image_id = resource.ret["Images"][0]["ImageId"]

    yield image_id


# --------------------------------------------------------------------------------
