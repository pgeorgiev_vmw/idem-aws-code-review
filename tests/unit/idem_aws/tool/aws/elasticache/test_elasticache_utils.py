def test_get_updated_cache_parameter_group(hub, mock_hub):
    """
    Test get_updated_cache_parameter_group() can correctly filter out only those parameters that needs to be modified
    """
    old_parameters = [
        {
            "ParameterName": "binding_protocol",
            "ParameterValue": "auto",
        },
        {
            "ParameterName": "chunk_size",
            "ParameterValue": "96",
        },
        {"ParameterName": "backlog_queue_limit", "ParameterValue": "1024"},
    ]

    # When value of only one required parameter id different
    new_parameters = [
        {
            "ParameterName": "binding_protocol",
            "ParameterValue": "ascii",
        },
        {
            "ParameterName": "chunk_size",
            "ParameterValue": "96",
        },
    ]

    expected_final_parameters = [
        {
            "ParameterName": "binding_protocol",
            "ParameterValue": "ascii",
        }
    ]

    mock_hub.tool.aws.elasticache.elasticache_utils.get_updated_cache_parameter_group = (
        hub.tool.aws.elasticache.elasticache_utils.get_updated_cache_parameter_group
    )

    final_parameters = mock_hub.tool.aws.elasticache.elasticache_utils.get_updated_cache_parameter_group(
        old_parameters, new_parameters
    )
    assert final_parameters == expected_final_parameters

    # If new parameter list is empty then no parameters should be changed
    new_parameters = []

    expected_final_parameters = []

    mock_hub.tool.aws.elasticache.elasticache_utils.get_updated_cache_parameter_group = (
        hub.tool.aws.elasticache.elasticache_utils.get_updated_cache_parameter_group
    )

    final_parameters = mock_hub.tool.aws.elasticache.elasticache_utils.get_updated_cache_parameter_group(
        old_parameters, new_parameters
    )
    assert final_parameters == expected_final_parameters

    # When values of all the parameters need to be changed
    new_parameters = [
        {
            "ParameterName": "binding_protocol",
            "ParameterValue": "ascii",
        },
        {
            "ParameterName": "chunk_size",
            "ParameterValue": "48",
        },
        {"ParameterName": "backlog_queue_limit", "ParameterValue": "2048"},
    ]

    expected_final_parameters = [
        {
            "ParameterName": "binding_protocol",
            "ParameterValue": "ascii",
        },
        {
            "ParameterName": "chunk_size",
            "ParameterValue": "48",
        },
        {"ParameterName": "backlog_queue_limit", "ParameterValue": "2048"},
    ]

    mock_hub.tool.aws.elasticache.elasticache_utils.get_updated_cache_parameter_group = (
        hub.tool.aws.elasticache.elasticache_utils.get_updated_cache_parameter_group
    )

    final_parameters = mock_hub.tool.aws.elasticache.elasticache_utils.get_updated_cache_parameter_group(
        old_parameters, new_parameters
    )
    assert final_parameters == expected_final_parameters
